import 'package:flutter/material.dart';

class ModalDialog {
  static onLoading(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(30),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: CircularProgressIndicator(),
                ),
                Text("Loading"),
              ],
            ),
          ),
        );
      },
    );
  }

  static errorMessage(BuildContext context, String title, String message) {
    // set up the buttons
    Widget loginButton = FlatButton(
      child: Text("Back to login"),
      onPressed: () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("/login", (Route route) => false);
      },
    );
    // set up the buttons
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("$title"),
      content: Text(message),
      actions: [
        message != "Unauthorized" ? continueButton : loginButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}