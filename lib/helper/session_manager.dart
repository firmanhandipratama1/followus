// import 'package:shared_preferences/shared_preferences.dart';
//
// class SessionManager {
//   final String authToken = "auth_token";
//
//   Future<void> setAuthToken(String authToken) async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.setString(this.authToken, authToken);
//   }
//
//   Future<void> removeAuthToken() async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.remove(this.authToken);
//   }
//
//   Future<String> getAuthToken() async {
//     final SharedPreferences pref = await SharedPreferences.getInstance();
//     String authToken = pref.getString(this.authToken) ?? null;
//     return authToken;
//   }
// }