import 'package:flutter/material.dart';

class TextForm extends StatelessWidget {
  final String hint;
  final bool isPassword;
  final TextInputType type;
  final FocusNode focusNode;
  final IconData icon;
  final Function onTapIcon;
  final int maxLine;
  final Color color;
  final Function onFieldSubmitted;
  final TextInputAction textInputAction;
  final Function onSaved;
  const TextForm({
    Key key,
    this.hint,
    this.isPassword = false,
    this.type = TextInputType.text,
    this.focusNode,
    this.icon,
    this.onTapIcon,
    this.maxLine = 1,
    this.color = Colors.white,
    this.onFieldSubmitted,
    this.textInputAction,
    this.onSaved
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: textInputAction,
      focusNode: focusNode,
      maxLines: maxLine,
      obscureText: isPassword,
      onFieldSubmitted: onFieldSubmitted,
      onSaved: onSaved,
      style: TextStyle(color: color),
      keyboardType: type,
      decoration: InputDecoration(
          suffixIcon: GestureDetector(onTap: onTapIcon, child: Icon(icon, color: Colors.grey,)),
          hintText: hint,
          hintStyle: TextStyle(color: Colors.grey),
          focusedBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: color)),
          enabledBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey))),
    );
  }
}
