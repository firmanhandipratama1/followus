import 'package:flutter/material.dart';

class ItemCard extends StatelessWidget {
  final double lebar;
  final double tinggiGambar;
  final String urlGambar;
  final String nama;
  final String kode;
  final String harga;
  final bool isBrowseitem;

  const ItemCard(
      {Key key,
      this.lebar,
      this.tinggiGambar,
      this.urlGambar,
      this.nama,
      this.kode,
      this.harga,
      this.isBrowseitem = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: (isBrowseitem == false)
          ? EdgeInsets.fromLTRB(2, 0, 2, 10)
          : EdgeInsets.only(bottom: 0),
      width: lebar,
      child: Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: tinggiGambar,
                width: double.infinity,
                // color: Colors.blue,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image(
                    image: AssetImage(urlGambar),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: 120,
                height: 60,
                // color: Colors.red,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      nama,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      kode,
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Colors.grey[600]),
                    )
                  ],
                ),
              ),
              Text(
                harga,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
              )
            ],
          ),
        ),
      ),
    );
  }
}
