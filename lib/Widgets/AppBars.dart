import 'package:flutter/material.dart';

class BlackAppBar extends StatelessWidget {
  final IconData icon1;
  final IconData icon2;
  final Function fIcon1;
  final Function fIcon2;
  final String judul;
  final bool isMyCart;
  const BlackAppBar(
      {Key key,
      this.icon1,
      this.icon2,
      this.fIcon1,
      this.fIcon2,
      this.judul,
      this.isMyCart = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.black,
      floating: true,
      elevation: 0,
      centerTitle: true,
      title: Text(judul),
      bottom: PreferredSize(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 5, 12, 12),
            child: (isMyCart == false)
                ? Row(
                    children: [
                      Expanded(
                          child: Container(
                        height: 40,
                        // margin: EdgeInsets.fromLTRB(12, 0, 12, 12),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextFormField(
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              hintText: 'Search',
                              hintStyle: TextStyle(color: Colors.grey),
                              prefixIcon: Icon(Icons.search),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide.none)),
                        ),
                      )),
                      SizedBox(width: 12),
                      GestureDetector(
                        onTap: fIcon1,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          child: Icon(
                            icon1,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      GestureDetector(
                        onTap: fIcon2,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          child: Icon(icon2, color: Colors.black54),
                        ),
                      ),
                    ],
                  )
                : Row(
                    children: [
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Pilih Semua Barang',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      )),
                      SizedBox(width: 5),
                      GestureDetector(
                        onTap: fIcon2,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          child: Icon(icon2, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
          ),
          preferredSize: Size.fromHeight(50)),
    );
  }
}
