import 'dart:io';

import 'package:flutter/material.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:follow_us/pickers/image_picker.dart';
import 'package:image_picker/image_picker.dart';

class WidgetsForSignUpAndEditProfile extends StatefulWidget {
  final bool isEditProfile;
  const WidgetsForSignUpAndEditProfile({Key key, this.isEditProfile = false})
      : super(key: key);

  @override
  _WidgetsForSignUpAndEditProfileState createState() =>
      _WidgetsForSignUpAndEditProfileState();
}

class _WidgetsForSignUpAndEditProfileState
    extends State<WidgetsForSignUpAndEditProfile> {
  bool isVisible = true;
  final _form = GlobalKey<FormState>();
  File _userImage;
  Map<String, String> authData = {
    'name': '',
    'phoneNumber': '',
    'email': '',
    'password': ''
  };

  void imageFile(File pickedImage){
    _userImage = pickedImage;
  }

  void toggle() {
    setState(() {
      isVisible = !isVisible;
    });
  }

  void onSubmit() {
    _form.currentState.save();
    print(authData['name']);
    print(authData['phoneNumber']);
    print(authData['email']);
    print(authData['password']);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: SafeArea(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SingleChildScrollView(
            child: Form(
              key: _form,
              child: Column(
                crossAxisAlignment: widget.isEditProfile == false
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text('Edit your credentials',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w600)),
                  SizedBox(
                    height: mediaQuery.height * 0.05,
                  ),
                  widget.isEditProfile == false
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Hello...!',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 42,
                                  fontWeight: FontWeight.w600),
                            ),
                            Text('Let\'s create a new account for you !',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontStyle: FontStyle.italic)),
                          ],
                        )
                      : UserImagePicker(imageFile),
                  SizedBox(
                    height: widget.isEditProfile == false
                        ? mediaQuery.height * 0.08
                        : mediaQuery.height * 0.02,
                  ),
                  TextForm(
                    hint: 'Full Name',
                    type: TextInputType.name,
                    onSaved: (value) {
                      authData['name'] = value;
                    },
                  ),
                  SizedBox(height: mediaQuery.height * 0.05),
                  TextForm(
                    hint: 'Phone Number',
                    type: TextInputType.phone,
                    onSaved: (value) {
                      authData['phoneNumber'] = value;
                    },
                  ),
                  SizedBox(height: mediaQuery.height * 0.05),
                  TextForm(
                    hint: 'Email',
                    type: TextInputType.emailAddress,
                    onSaved: (value) {
                      authData['email'] = value;
                    },
                  ),
                  SizedBox(height: mediaQuery.height * 0.05),
                  TextForm(
                    hint: 'Password',
                    isPassword: isVisible ? true : false,
                    type: TextInputType.text,
                    icon: isVisible ? Icons.visibility : Icons.visibility_off,
                    onTapIcon: toggle,
                    onSaved: (value) {
                      authData['password'] = value;
                    },
                  ),
                  SizedBox(height: mediaQuery.height * 0.06),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: mediaQuery.width * 0.5,
                          child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30)),
                              onPressed: onSubmit,
                              color: Colors.white,
                              child: Text(
                                widget.isEditProfile == false
                                    ? 'SignUp'
                                    : 'Save Changes',
                                style: TextStyle(fontSize: 16),
                              )),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        widget.isEditProfile == false
                            ? GestureDetector(
                                onTap: () => Navigator.pop(context),
                                child: Container(
                                  // color: Colors.red,
                                  child: RichText(
                                    text: TextSpan(
                                        text: 'Already have an account?',
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 13),
                                        children: [
                                          TextSpan(
                                              text: ' Sign In',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white))
                                        ]),
                                  ),
                                ),
                              )
                            : SizedBox()
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        )),
      ),
    );
  }
}
