import 'package:flutter/material.dart';

class SelectSize extends StatefulWidget {
  @override
  _SelectSizeState createState() => _SelectSizeState();
}

class _SelectSizeState extends State<SelectSize> {
  int _selectedIndex = 0;
  List<String> ukuran = ['S', 'M', 'L', 'XL', 'XXL'];

  Widget _selectSize(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        print(_selectedIndex);
      },
      child: Container(
        width: 60,
        height: 45,
        decoration: BoxDecoration(
            color: _selectedIndex == index ? Colors.black87 : Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: _selectedIndex == index
                ? Border.all(width: 0)
                : Border.all(width: 1, color: Colors.grey)),
        child: Center(
          child: Text(ukuran[index],
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color:
                      _selectedIndex == index ? Colors.white : Colors.black54)),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: ukuran
            .asMap()
            .entries
            .map((MapEntry map) => _selectSize(map.key))
            .toList());
  }
}
