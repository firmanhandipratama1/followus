import 'package:flutter/material.dart';

class FavouriteIconButton extends StatefulWidget {
  @override
  _FavouriteIconButtonState createState() => _FavouriteIconButtonState();
}

class _FavouriteIconButtonState extends State<FavouriteIconButton>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _colorAnimation;
  Animation<double> _sizeAnimation;
  bool isFav = false;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));

    _colorAnimation =
        ColorTween(begin: Colors.grey, end: Colors.red).animate(_controller);

    _sizeAnimation = TweenSequence(<TweenSequenceItem<double>>[
      TweenSequenceItem(tween: Tween<double>(begin: 20, end: 40), weight: 50),
      TweenSequenceItem(tween: Tween<double>(begin: 40, end: 20), weight: 50),
    ]).animate(_controller);

    _colorAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          isFav = true;
        });
      } else if (status == AnimationStatus.dismissed) {
        setState(() {
          isFav = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, _) {
        return GestureDetector(
          onTap: () {
            isFav ? _controller.reverse() : _controller.forward();
          },
          child: Icon(
            Icons.favorite,
            color: _colorAnimation.value,
            // size: _sizeAnimation.value,
          ),
        );
      },
    );
  }
}
