import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:follow_us/models/models.dart';

class Carouselna extends StatelessWidget {
  final double tinggi;
  final double aspectRatio;
  final double viewPort;
  final bool autoPlay;
  final String imgURL;
  final int length;
  final bool isHomePage;
  final bool enlargeCenter;
  const Carouselna(
      {Key key,
      this.tinggi,
      this.aspectRatio,
      this.viewPort,
      this.length,
      this.autoPlay,
      this.isHomePage,
      this.enlargeCenter,
      this.imgURL})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.blue,
      margin: EdgeInsets.only(top: (isHomePage == true) ? 20 : 0),
      child: CarouselSlider.builder(
          options: CarouselOptions(
            height: tinggi,
            aspectRatio: aspectRatio,
            viewportFraction: viewPort,
            initialPage: 0,
            enableInfiniteScroll: true,
            reverse: false,
            autoPlay: autoPlay,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            enlargeCenterPage: enlargeCenter,
            scrollDirection: Axis.horizontal,
          ),
          itemCount: length,
          itemBuilder: (context, index) {
            if (isHomePage == true) {
              return Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Container(
                  height: 180,
                  width: 300,
                  decoration: BoxDecoration(
                      // color: Colors.red,
                      borderRadius: BorderRadius.circular(10)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image(
                      image: AssetImage(karosel[index].imgURL),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              );
            } else {
              return Container(
                color: Colors.red,
                height: 80,
                width: double.infinity,
                child: Image(
                  image: AssetImage(imgURL),
                  fit: BoxFit.cover,
                ),
              );
            }
          }),
    );
  }
}
