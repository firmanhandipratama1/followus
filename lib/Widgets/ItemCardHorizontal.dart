import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:follow_us/models/models.dart';

class ItemCardHorizontal extends StatelessWidget {
  final Color warna;
  final double tinggi;
  final bool isAccount;

  const ItemCardHorizontal(
      {Key key, this.warna, this.tinggi = 510, this.isAccount = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: warna,
      height: tinggi,
      child: ListView.builder(
        itemCount: merchandises.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            // color: Colors.blue,
            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
            width: double.infinity,
            // height: 50,
            child: Card(
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 100,
                        height: 90,
                        decoration: BoxDecoration(
                            // color: Colors.green,
                            borderRadius: BorderRadius.circular(5)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image(
                            image: AssetImage(merchandises[index].imgURL),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          height: MediaQuery.of(context).size.width * 0.25,
                          // color: Colors.green,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                merchandises[index].name,
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Expanded(
                                child: Text('(1 x Size M)',
                                    style: TextStyle(
                                      fontSize: 11,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w600,
                                    )),
                              ),
                              Text('IDR ${merchandises[index].price}',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600)),
                            ],
                          )),
                      isAccount
                          ? Expanded(
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.width * 0.25,
                                // color: Colors.red,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Icon(
                                      MdiIcons.trashCanOutline,
                                      size: 22,
                                      color: Colors.grey[600],
                                    ),
                                    Icon(
                                      MdiIcons.pencilOutline,
                                      color: Colors.grey[600],
                                      size: 22,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  )),
            ),
          );
        },
      ),
    );
  }
}
