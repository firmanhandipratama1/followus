import 'package:flutter/material.dart';

class Quantity extends StatefulWidget {
  @override
  _QuantityState createState() => _QuantityState();
}

class _QuantityState extends State<Quantity> {
int _number = 0;
Widget _quantity() {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              if (_number > 0) {
                _number--;
              }
            });
          },
          child: Container(
            padding: EdgeInsets.only(
              bottom: 0,
            ),
            width: 20,
            height: 35,
            decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(6),
                    bottomLeft: Radius.circular(6))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  '-',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          width: 110,
          height: 35,
          decoration: BoxDecoration(
            color: Colors.black87,
          ),
          child: Center(
            child: Text(
              '$_number',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              _number++;
            });
          },
          child: Container(
            padding: EdgeInsets.only(top: 4),
            width: 20,
            height: 35,
            decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(6),
                    bottomRight: Radius.circular(6))),
            child: Text(
              '+',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
  
    return _quantity();
  }
}