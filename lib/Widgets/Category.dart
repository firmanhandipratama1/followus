import 'package:flutter/material.dart';
import 'package:follow_us/Pages/BrowseItems.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Categories extends StatelessWidget {
  final List<IconData> icons = [
    MdiIcons.allInclusive,
    MdiIcons.react,
    MdiIcons.kodi,
    MdiIcons.openInNew
  ];
  final List<String> kategori = ['All', 'Tops', 'Bottom', 'Accessories'];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 95,
      // color: Colors.green,
      width: double.infinity,
      child: ListView.builder(
        itemCount: icons.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: index == 0
                ? () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BrowseItems()));
                  }
                : () {},
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(12, 4, 12, 8),
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10)),
                  width: 65,
                  height: 65,
                  child: Icon(icons[index], color: Colors.white),
                ),
                Text(
                  kategori[index],
                  style: TextStyle(fontWeight: FontWeight.w600),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
