import 'package:flutter/material.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:follow_us/models/models.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ItemDetails extends StatefulWidget {
  final ItemModels merchandises;

  const ItemDetails({Key key, this.merchandises}) : super(key: key);

  @override
  _ItemDetailsState createState() => _ItemDetailsState();
}

class _ItemDetailsState extends State<ItemDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: [
          BlackAppBar(
            fIcon1: () {},
            fIcon2: () {},
            icon1: MdiIcons.homeOutline,
            icon2: MdiIcons.cartOutline,
            judul: 'Follow Us',
          ),
          SliverToBoxAdapter(
            child: Carouselna(
              aspectRatio: 1,
              autoPlay: false,
              imgURL: widget.merchandises.imgURL,
              isHomePage: false,
              enlargeCenter: false,
              length: 1,
              tinggi: 280,
              viewPort: 1,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                          child: Container(
                        // color: Colors.red,
                        child: Text(
                          widget.merchandises.name,
                          style: TextStyle(fontSize: 20),
                        ),
                      )),
                      SizedBox(width: 60),
                      FavouriteIconButton()
                    ],
                  ),
                  Text(
                    widget.merchandises.code,
                    style: TextStyle(color: Colors.grey[500]),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'IDR ${widget.merchandises.price}',
                    style: TextStyle(fontSize: 26, fontWeight: FontWeight.w600),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(height: 8),
          ),
          SliverToBoxAdapter(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Description',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    widget.merchandises.description,
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'Read more',
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 10,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Select Size',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
                  SizedBox(
                    height: 10,
                  ),
                  SelectSize(),
                  SizedBox(
                    height: 15,
                  ),
                  Text('Quantity',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
                  SizedBox(
                    height: 10,
                  ),
                  Quantity(),
                  SizedBox(
                    height: 24,
                  ),
                  FlatButton(
                      padding: EdgeInsets.symmetric(vertical: 12),
                      color: Colors.black87,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            MdiIcons.cartOutline,
                            color: Colors.white,
                            size: 24,
                          ),
                          SizedBox(width: 2),
                          Text('Add to Cart',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20))
                        ],
                      ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
