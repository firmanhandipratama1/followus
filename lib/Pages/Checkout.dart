import 'package:flutter/material.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CheckoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            centerTitle: true,
            backgroundColor: Colors.black87,
            title: Text(
              'Checkout',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.16,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Address',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 4),
                  Row(
                    children: [
                      Text('Firman Handi Pratama',
                          style: TextStyle(fontWeight: FontWeight.w500)),
                      SizedBox(width: 4),
                      Expanded(
                          child: Text('(+628182919290)',
                              style: TextStyle(color: Colors.grey))),
                      Icon(MdiIcons.pencilOutline,
                          size: 20, color: Colors.grey[700])
                    ],
                  ),
                  const SizedBox(height: 4),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    // color: Colors.red,
                    child: Text(
                      'Jalan Pengajaran 5000212 Desa Konohagakure  Kabupaten Suratokyo kecamatan Pyongyakarta Provinsi London Timur, di depan ramen ichiraku',
                      style: TextStyle(fontSize: 12),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: ItemCardHorizontal(
              warna: Colors.red,
              tinggi: 300,
            ),
          )
        ],
      ),
    );
  }
}
