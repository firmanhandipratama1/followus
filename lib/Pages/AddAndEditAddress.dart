import 'package:flutter/material.dart';
import 'package:follow_us/Widgets/widgets.dart';

class AddAndEditAddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        centerTitle: true,
        title: Text(
          'Add Address',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 12,
            ),
            TextForm(
              color: Colors.black,
              hint: 'Place (ex : Home, Office)',
            ),
            const SizedBox(
              height: 24,
            ),
            TextForm(
              color: Colors.black,
              hint: 'Addressee (ex : David Williamson)',
            ),
            const SizedBox(
              height: 24,
            ),
            Expanded(
                          child: TextForm(
                hint: 'Address',
                maxLine: null,
                color: Colors.black,
                type: TextInputType.multiline,
              ),
            ),
            Container(
              width: double.infinity,
              child: FlatButton(
                color: Colors.black,
                onPressed: () {},
                child: Text(
                  'Save Address',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
