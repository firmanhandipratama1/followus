import 'package:flutter/material.dart';
import 'package:follow_us/Pages/pages.dart';
import 'package:follow_us/Widgets/widgets.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(bottom: 24),
              width: double.infinity,
              // height: 450,
              color: Colors.black87,
              child: SafeArea(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 16.0, top: 8),
                          child: GestureDetector(
                            onTap: () {},
                            child: Text('Logout',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700)),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 12.0, bottom: 16),
                      child: CircleAvatar(
                        radius: 50,
                        backgroundImage: AssetImage(
                            'assets/images/Profile_Picture_Cropped.jpg'),
                      ),
                    ),
                    Text(
                      'Alex Moumukala Morgan',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                          fontWeight: FontWeight.w400),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text('alexMou@gmail.com',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14,
                            fontWeight: FontWeight.w300)),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.7,
                      // color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          _Buttons(
                            title: 'Address',
                            icons: Icons.location_on_rounded,
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => ManageAddressPage()));
                            },
                          ),
                          _Buttons(
                            title: 'Edit Profile',
                            icons: Icons.edit,
                            onTap: () {},
                          ),
                          _Buttons(
                            title: 'Wishlist',
                            icons: Icons.favorite,
                            onTap: () {},
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 14.0, top: 20),
                  child: Text(
                    'Recent Transactions',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ),
                ItemCardHorizontal(
                  warna: Colors.transparent,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _Buttons extends StatelessWidget {
  final String title;
  final Function onTap;
  final IconData icons;

  const _Buttons({Key key, this.title, this.onTap, this.icons})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: Colors.grey[700],
                borderRadius: BorderRadius.circular(6)),
            child: Icon(icons, color: Colors.white),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 13),
          )
        ],
      ),
    );
  }
}
