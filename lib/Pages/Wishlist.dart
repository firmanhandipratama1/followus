import 'package:flutter/material.dart';
import 'package:follow_us/Pages/MyCart.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:follow_us/models/models.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'HomePage.dart';

class Wishlist extends StatefulWidget {
  @override
  _WishlistState createState() => _WishlistState();
}

class _WishlistState extends State<Wishlist> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          BlackAppBar(
            judul: 'My Wishlist',
            icon1: MdiIcons.homeOutline,
            icon2: MdiIcons.cartOutline,
            fIcon1: () => Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => HomePage())),
            fIcon2: () => Navigator.push(
                context, MaterialPageRoute(builder: (_) => MyCart())),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(bottom: 20),
              width: double.infinity,
              // color: Colors.red,
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: merchandises.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    // color: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    width: double.infinity,
                    // height: 50,
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 100,
                                  height: 80,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: Image(
                                      image: AssetImage(
                                          merchandises[index].imgURL),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8),
                                Container(
                                    width: 160,
                                    // color: Colors.green,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          merchandises[index].name,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 6,
                                        ),
                                        Text('IDR ${merchandises[index].price}',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600))
                                      ],
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 100,
                                  child: OutlineButton(
                                    borderSide:
                                        BorderSide(color: Colors.grey[400]),
                                    onPressed: () {},
                                    child: Row(
                                      children: [
                                        Icon(
                                          MdiIcons.trashCanOutline,
                                          color: Colors.grey[500],
                                          size: 20,
                                        ),
                                        Text(
                                          'Delete',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15),
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6)),
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Expanded(
                                  child: FlatButton(
                                      color: Colors.black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(6)),
                                      onPressed: () {},
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            MdiIcons.cartOutline,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                          Text(
                                            'Add to Cart',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                            ),
                                          )
                                        ],
                                      )),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
