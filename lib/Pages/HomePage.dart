import 'package:flutter/material.dart';
import 'pages.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:follow_us/models/models.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          child: CustomScrollView(
            slivers: [
              BlackAppBar(
                fIcon1: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => Wishlist()));
                },
                icon1: MdiIcons.heartOutline,
                fIcon2: () {
                  
                },
                icon2: MdiIcons.exitToApp,
                judul: 'Follow Us',
              ),
              SliverToBoxAdapter(
                child: Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 110,
                      color: Colors.black,
                    ),
                    Carouselna(
                      length: karosel.length,
                      aspectRatio: 16 / 9,
                      autoPlay: true,
                      isHomePage: true,
                      enlargeCenter: true,
                      tinggi: 180,
                      viewPort: 0.8,
                    )
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, bottom: 6),
                      child: Text(
                        'Category',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w700),
                      ),
                    ),
                    Categories()
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, bottom: 0),
                      child: Text('New Arrivals',
                          style: TextStyle(
                              fontSize: 22, fontWeight: FontWeight.w700)),
                    ),
                    Container(
                      height: 260,
                      // padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                      // color: Colors.red,
                      width: double.infinity,
                      child: ListView.builder(
                        itemCount: merchandises.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          ItemModels _merchandises = merchandises[index];
                          return GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => ItemDetails(
                                  merchandises: _merchandises,
                                ),
                              ),
                            ),
                            child: ItemCard(
                              nama: _merchandises.name,
                              harga: 'IDR${_merchandises.price}',
                              kode: _merchandises.code,
                              urlGambar: _merchandises.imgURL,
                              tinggiGambar: 130,
                              lebar: 160,
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
