import 'package:flutter/material.dart';
import 'pages.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final passwordFocus = FocusNode();
  final _form = GlobalKey<FormState>();
  // String emailData;
  // String passwordData;

  Map<String, String> authData = {'email': '', 'password': ''};
  bool isLoading = false;

  bool make = true;
  void toogle() {
    setState(() {
      make = !make;
    });
  }

  void onSubmit() {
    _form.currentState.save();
    print(authData['email']);
    print(authData['password']);
  }

  @override
  void dispose() {
    passwordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: Form(
          key: _form,
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        'Follow Us',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    SizedBox(height: mediaQuery.height * 0.1),
                    Container(
                      width: mediaQuery.width * 0.6,
                      // color: Colors.red,
                      child: Text('Welcome Back!',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 42,
                              fontWeight: FontWeight.w700)),
                    ),
                    SizedBox(height: mediaQuery.height * 0.1),
                    TextForm(
                      hint: 'Email',
                      textInputAction: TextInputAction.next,
                      onSaved: (value) {
                        authData['email'] = value;
                      },
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(passwordFocus),
                    ),
                    SizedBox(height: mediaQuery.height * 0.05),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        TextForm(
                          type: TextInputType.emailAddress,
                          hint: 'Password',
                          focusNode: passwordFocus,
                          isPassword: make,
                          onSaved: (value) {
                            authData['password'] = value;
                          },
                          icon: make
                              ? MdiIcons.eyeOutline
                              : MdiIcons.eyeOffOutline,
                          onTapIcon: toogle,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Forgot Password?',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontWeight: FontWeight.w600),
                        )
                      ],
                    ),
                    SizedBox(height: mediaQuery.height * 0.1),
                    Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          isLoading
                              ? CircularProgressIndicator()
                              : Container(
                                  width: mediaQuery.width * 0.5,
                                  // color: Colors.red,
                                  child: FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30)),
                                      onPressed: onSubmit,
                                      color: Colors.white,
                                      child: Text(
                                        'Sign In',
                                        style: TextStyle(fontSize: 16),
                                      )),
                                ),
                          SizedBox(
                            height: 6,
                          ),
                          GestureDetector(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => SignUpPage())),
                            child: Container(
                              child: RichText(
                                text: TextSpan(
                                    text: 'New User?',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 13),
                                    children: [
                                      TextSpan(
                                          text: ' Create Account',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white))
                                    ]),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
