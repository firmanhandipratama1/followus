import 'package:flutter/material.dart';
import 'package:follow_us/Pages/pages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ManageAddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        centerTitle: true,
        title: Text(
          'Manage Your Address',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
              child: OutlinedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AddAndEditAddressPage()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.add_circle_outline, color: Colors.grey[600]),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      'Add New Address',
                      style: TextStyle(color: Colors.grey[600]),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.width * 1.36,
              // color: Colors.red,
              child: ListView.builder(
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    // color: Colors.blue,
                    padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    width: double.infinity,
                    // height: 50,
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  'Home',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )),
                                Icon(
                                  MdiIcons.trashCanOutline,
                                  size: 22,
                                  color: Colors.grey[600],
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Icon(
                                  MdiIcons.pencilOutline,
                                  color: Colors.grey[600],
                                  size: 22,
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            Text(
                              'Firman Handi Pratama',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            Text(
                              'Jalan Pengajaran 5000212 Desa Konohagakure  Kabupaten Suratokyo kecamatan Pyongyakarta Provinsi London Timur, di depan ramen ichiraku',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
