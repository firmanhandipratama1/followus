import 'package:flutter/material.dart';
import 'pages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  final _widgetOptions = <Widget>[
    HomePage(),
    MyCart(),
    Container(child: Text("Help")),
    Container(
      child: Text("Account"),
    )
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(MdiIcons.homeOutline), label: "Home"),
          BottomNavigationBarItem(
              icon: Icon(MdiIcons.cartOutline), label: "Cart"),
          BottomNavigationBarItem(
              icon: Icon(MdiIcons.informationOutline), label: "Help"),
          BottomNavigationBarItem(
              icon: Icon(MdiIcons.accountCircleOutline),
              label: "Account"),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.grey[400],
      ),
    );
  }
}
