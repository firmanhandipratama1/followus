import 'package:flutter/material.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:follow_us/models/models.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MyCart extends StatelessWidget {
  void showDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (_) => Card(
        child:
            Column(
              children: [
                        
              ],
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          BlackAppBar(
              judul: 'My Cart',
              isMyCart: true,
              icon2: MdiIcons.trashCanOutline),
          SliverToBoxAdapter(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 20),
              // color: Colors.red,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: merchandises.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    // color: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    // height: 50,
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 100,
                                height: 90,
                                decoration: BoxDecoration(
                                    // color: Colors.green,
                                    borderRadius: BorderRadius.circular(5)),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Image(
                                    image:
                                        AssetImage(merchandises[index].imgURL),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              SizedBox(width: 8),
                              Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.45,
                                  height:
                                      MediaQuery.of(context).size.width * 0.25,
                                  // color: Colors.green,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        merchandises[index].name,
                                        style: TextStyle(fontSize: 16),
                                      ),
                                      SizedBox(
                                        height: 2,
                                      ),
                                      Expanded(
                                        child: Text('(1 x Size M)',
                                            style: TextStyle(
                                              fontSize: 11,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w600,
                                            )),
                                      ),
                                      Text('IDR ${merchandises[index].price}',
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600)),
                                    ],
                                  )),
                              Expanded(
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.width * 0.25,
                                  // color: Colors.red,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        MdiIcons.trashCanOutline,
                                        size: 22,
                                        color: Colors.grey[600],
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          showDialog(context);
                                        },
                                        child: Icon(
                                          MdiIcons.pencilOutline,
                                          color: Colors.grey[600],
                                          size: 22,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
