import 'package:flutter/material.dart';
import 'pages.dart';
import 'package:follow_us/Widgets/widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:follow_us/models/models.dart';

class BrowseItems extends StatefulWidget {
  @override
  _BrowseItemsState createState() => _BrowseItemsState();
}

class _BrowseItemsState extends State<BrowseItems> {
  List<String> _categoryList = ['All', 'Tops', 'Bottoms', 'Accessories'];
  int _selectedIndex = 0;

  Widget _chooseCategory(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: _selectedIndex == index ? Colors.black87 : Colors.grey[200],
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
                width: _selectedIndex == index ? 0 : 1,
                color: Colors.grey[400])),
        child: Center(
            child: Text(
          _categoryList[index],
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: _selectedIndex == index ? Colors.white : Colors.grey[500]),
        )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: [
          BlackAppBar(
            icon1: MdiIcons.homeOutline,
            icon2: MdiIcons.cartOutline,
            fIcon1: () => Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => HomePage())),
            judul: 'Follow Us',
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: _categoryList
                    .asMap()
                    .entries
                    .map((MapEntry map) => _chooseCategory(map.key))
                    .toList(),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              // color: Colors.red,
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.71,
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 0.7),
                  itemCount: merchandises.length,
                  itemBuilder: (context, index) {
                    ItemModels _merchandises = merchandises[index];
                    return ItemCard(
                      isBrowseitem: true,
                      nama: _merchandises.name,
                      harga: 'IDR${_merchandises.price}',
                      kode: _merchandises.code,
                      urlGambar: _merchandises.imgURL,
                      tinggiGambar: 130,
                      // lebar: 160,
                    );
                  }),
            ),
          )
        ],
      ),
    );
  }
}
