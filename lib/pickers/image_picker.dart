import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserImagePicker extends StatefulWidget {
  final void Function(File pickedImage) imageFilefn;

  const UserImagePicker(this.imageFilefn);

  @override
  _UserImagePickerState createState() => _UserImagePickerState();
}

class _UserImagePickerState extends State<UserImagePicker> {
  File _pickedImage;

  void pickImage() async {
    final pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);

    setState(() {
      _pickedImage = File(pickedFile.path);
    });
    widget.imageFilefn(_pickedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.bottomCenter,
        children: [
          Center(
            child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.grey,
                backgroundImage:
                    _pickedImage == null ? null : FileImage(_pickedImage)),
          ),
          Positioned(
            bottom: -10,
            child: GestureDetector(
              onTap: pickImage,
              child: Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                      color: Colors.white, shape: BoxShape.circle),
                  child: Icon(
                    Icons.camera_alt,
                    size: 17,
                  )),
            ),
          ),
        ]);
  }
}
