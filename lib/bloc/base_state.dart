abstract class BaseState {}

class StateInitial extends BaseState {}

class StateLoading extends BaseState {}

class StateFailure extends BaseState {
  final String errorMessage;

  StateFailure(this.errorMessage);
}
