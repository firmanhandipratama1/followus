import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:bloc/bloc.dart';
import 'package:follow_us/models/response/response_product_list.dart';
import 'package:follow_us/models/response/response_single_product.dart';
import 'package:follow_us/services/product_repository.dart';
import 'package:follow_us/bloc/base_state.dart';

part 'product_event.dart';

part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  @override
  ProductState get initialState => ProductInitial();

  @override
  Stream<ProductState> mapEventToState(
      ProductEvent event,
      ) async* {
    ProductRepository repository = ProductRepository();
    if (event is LoadProductList) {
      yield ProductLoading();
      ResponseProductList responseProductList = await repository.getProductList();
      yield ProductListLoaded(responseProductList: responseProductList);
    }
    else if (event is LoadSingleProduct) {
      yield ProductLoading();
      ResponseSingleProduct responseSingleProduct = await repository.getSingleProduct(event.productId);
      yield SingleProductLoaded(responseSingleProduct : responseSingleProduct);
    } else {
      yield ProductFailed("");
    }
  }
}
