part of 'product_bloc.dart';

@immutable
abstract class ProductState extends BaseState {}

class ProductInitial extends ProductState {
  @override
  List<Object> get props => [];
}

class ProductLoading extends ProductState {
}

class ProductListLoaded extends ProductState {
  final ResponseProductList responseProductList;

  ProductListLoaded({this.responseProductList});
}

class SingleProductLoaded extends ProductState {
  final ResponseSingleProduct responseSingleProduct;

  SingleProductLoaded({this.responseSingleProduct});
}

class ProductFailed extends ProductState {
  final String message;
  ProductFailed(this.message);
}