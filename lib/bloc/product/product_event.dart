part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();

  @override
  List<Object> get props => [];
}

class LoadProductList extends ProductEvent {}

class LoadSingleProduct extends ProductEvent {
  final String productId;

  LoadSingleProduct(this.productId);
}
