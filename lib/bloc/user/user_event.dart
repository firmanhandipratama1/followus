part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object> get props => [];
}

class PostLogin extends UserEvent {
  final RequestLogin requestLogin;

  PostLogin(this.requestLogin);
}

class PostRegister extends UserEvent {
  final RequestRegister requestRegister;

  PostRegister(this.requestRegister);
}
