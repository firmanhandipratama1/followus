part of 'user_bloc.dart';

@immutable
abstract class UserState extends BaseState {}

class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

class UserLoading extends UserState {
}

class UserLoggedIn extends UserState {
  final ResponseLogin responseLogin;

  UserLoggedIn({this.responseLogin});
}

class UserRegistered extends UserState {
  final ResponseRegister responseRegister;

  UserRegistered({this.responseRegister});
}

class UserFailed extends UserState {
  final String message;
  UserFailed(this.message);
}