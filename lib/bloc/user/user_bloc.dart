import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:bloc/bloc.dart';
import 'package:follow_us/bloc/base_state.dart';
import 'package:follow_us/models/request/request_login.dart';
import 'package:follow_us/models/request/request_register.dart';
import 'package:follow_us/models/response/response_login.dart';
import 'package:follow_us/models/response/response_register.dart';
import 'package:follow_us/services/user_repository.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  @override
  UserState get initialState => UserInitial();

  @override
  Stream<UserState> mapEventToState(
      UserEvent event,
      ) async* {
    UserRepository repository = UserRepository();
    if (event is PostLogin) {
      yield UserLoading();
      ResponseLogin responseLogin = await repository.goLogin(event.requestLogin);
      yield UserLoggedIn(responseLogin: responseLogin);
    }
    else if (event is PostRegister) {
      yield UserLoading();
      ResponseRegister responseRegister = await repository.goRegister(event.requestRegister);
      yield UserRegistered(responseRegister: responseRegister);
    } else {
      yield UserFailed("");
    }
  }
}
