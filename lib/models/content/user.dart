class User {
  int id;
  String userId;
  String username;
  String pwd;
  String email;
  String noTlp;
  String address;
  bool status;
  String createdAt;
  String updatedAt;

  User(
      {this.id,
        this.userId,
        this.username,
        this.pwd,
        this.email,
        this.noTlp,
        this.address,
        this.status,
        this.createdAt,
        this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    username = json['username'];
    pwd = json['pwd'];
    email = json['email'];
    noTlp = json['no_tlp'];
    address = json['address'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['username'] = this.username;
    data['pwd'] = this.pwd;
    data['email'] = this.email;
    data['no_tlp'] = this.noTlp;
    data['address'] = this.address;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}