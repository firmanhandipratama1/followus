class Address {
  int id;
  int idUser;
  String provinsi;
  String kabupaten;
  String kecamatan;
  String kelurahan;
  String address;
  bool status;
  String createdAt;
  String updatedAt;

  Address(
      {this.id,
        this.idUser,
        this.provinsi,
        this.kabupaten,
        this.kecamatan,
        this.kelurahan,
        this.address,
        this.status,
        this.createdAt,
        this.updatedAt});

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUser = json['id_user'];
    provinsi = json['provinsi'];
    kabupaten = json['kabupaten'];
    kecamatan = json['kecamatan'];
    kelurahan = json['kelurahan'];
    address = json['address'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user'] = this.idUser;
    data['provinsi'] = this.provinsi;
    data['kabupaten'] = this.kabupaten;
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['address'] = this.address;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}