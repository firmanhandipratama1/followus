class RequestProduct {
  String judul;
  String deskripsi;
  String berat;
  int katId;
  String hargaNormal;
  String diskon;
  String hargaDiskon;
  int stok;
  String foto;
  bool status;
  String createdAt;
  String updatedAt;

  RequestProduct(
      {this.judul,
      this.deskripsi,
      this.berat,
      this.katId,
      this.hargaNormal,
      this.diskon,
      this.hargaDiskon,
      this.stok,
      this.foto,
      this.status,
      this.createdAt,
      this.updatedAt});

  RequestProduct.fromJson(Map<String, dynamic> json) {
    judul = json['judul'];
    deskripsi = json['deskripsi'];
    berat = json['berat'];
    katId = json['kat_id'];
    hargaNormal = json['harga_normal'];
    diskon = json['diskon'];
    hargaDiskon = json['harga_diskon'];
    stok = json['stok'];
    foto = json['foto'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['judul'] = this.judul;
    data['deskripsi'] = this.deskripsi;
    data['berat'] = this.berat;
    data['kat_id'] = this.katId;
    data['harga_normal'] = this.hargaNormal;
    data['diskon'] = this.diskon;
    data['harga_diskon'] = this.hargaDiskon;
    data['stok'] = this.stok;
    data['foto'] = this.foto;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}
