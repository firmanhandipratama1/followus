class RequestAddress {
  String kecamatan;
  String kelurahan;
  String address;
  String status;

  RequestAddress({this.kecamatan, this.kelurahan, this.address, this.status});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['address'] = this.address;
    data['status'] = this.status;
    return data;
  }
}