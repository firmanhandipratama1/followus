class RequestLogin {
  String userId;
  String password;

  RequestLogin({this.userId, this.password});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['password'] = this.password;
    return data;
  }
}