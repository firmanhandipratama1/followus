import 'package:follow_us/models/content/content.dart';
import 'package:follow_us/models/content/user.dart';

class ResponseRegister {
  String message;
  User data;
  String errMessage;

  ResponseRegister({this.data});

  ResponseRegister.withMessage(this.errMessage);


  ResponseRegister.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new User.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}