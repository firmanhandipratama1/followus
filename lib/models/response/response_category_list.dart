import 'package:follow_us/models/content/category.dart';

class ResponseCategoryList {
  List<Category> data;
  String errMessage;

  ResponseCategoryList({this.data});
  ResponseCategoryList.withMessage(this.errMessage);

  ResponseCategoryList.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Category>();
      json['data'].forEach((v) {
        data.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}