import 'package:follow_us/models/content/user_login.dart';

class ResponseLogin {
  String message;
  List<UserLogin> data;
  String token;
  String errMessage;

  ResponseLogin({this.message, this.data, this.token});
  ResponseLogin.withMessage(this.errMessage);

  ResponseLogin.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    if (json['data'] != null) {
      data = new List<UserLogin>();
      json['data'].forEach((v) {
        data.add(new UserLogin.fromJson(v));
      });
    }
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['token'] = this.token;
    return data;
  }
}