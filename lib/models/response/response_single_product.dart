import 'package:follow_us/models/content/product.dart';

class ResponseSingleProduct {
  Product data;
  String errMessage;

  ResponseSingleProduct({this.data});

  ResponseSingleProduct.withMessage(this.errMessage);


  ResponseSingleProduct.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Product.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}