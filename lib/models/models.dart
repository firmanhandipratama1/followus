class ItemModels {
  final String name;
  final String code;
  final int price;
  final String description;
  final String imgURL;

  ItemModels({this.name, this.code, this.price, this.description, this.imgURL});
}

List<ItemModels> karosel = [
  ItemModels(imgURL: 'assets/images/banner1.jpg'),
  ItemModels(imgURL: 'assets/images/banner2.jpg'),
  ItemModels(imgURL: 'assets/images/banner3.jpg'),
];

List<ItemModels> merchandises = [
  ItemModels(
      name: 'New Balance Chrones 3000',
      price: 450000,
      code: 'Adventure',
      description:
          'I played this video every night to help me fall asleep, but then something happened with the video because I swear it went private or something. Anyways I’m happy this mix is back now, no other playlists or videos had all of these song or even just the order. I’m happy this exists truly.',
      imgURL: 'assets/images/jeremy.jpg'),
  ItemModels(
      name: 'Ini sengaja namanya dibikin panjang',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/nike2.jpg'),
  ItemModels(
      name: 'New Balance 3',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/felipe.jpg'),
  ItemModels(
      name: 'New Balance 4',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/variation1.jpg'),
  ItemModels(
      name: 'New Balance Chrones 3000',
      price: 450000,
      code: 'Adventure',
      description:
      'I played this video every night to help me fall asleep, but then something happened with the video because I swear it went private or something. Anyways I’m happy this mix is back now, no other playlists or videos had all of these song or even just the order. I’m happy this exists truly.',
      imgURL: 'assets/images/jeremy.jpg'),
  ItemModels(
      name: 'Ini sengaja namanya dibikin panjang',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/nike2.jpg'),
  ItemModels(
      name: 'New Balance 3',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/felipe.jpg'),
  ItemModels(
      name: 'New Balance 4',
      price: 450000,
      code: 'Adventure',
      description: 'wkwkwkwk',
      imgURL: 'assets/images/variation1.jpg'),
];
