import 'package:follow_us/models/request/request_login.dart';
import 'package:follow_us/models/request/request_register.dart';
import 'package:follow_us/models/response/response_login.dart';
import 'package:follow_us/models/response/response_register.dart';
import 'package:follow_us/services/provider.dart';

class UserRepository {
  final Provider _provider = Provider();

  Future<ResponseLogin> goLogin(RequestLogin requestLogin) =>
      _provider.postLogin(requestLogin);

  Future<ResponseRegister> goRegister(RequestRegister requestRegister) =>
      _provider.postRegister(requestRegister);
}
