import 'package:dio/dio.dart';
import 'package:follow_us/models/request/request_login.dart';
import 'package:follow_us/models/request/request_register.dart';
import 'package:follow_us/models/response/response_address_list.dart';
import 'package:follow_us/models/response/response_category_list.dart';
import 'package:follow_us/models/response/response_login.dart';
import 'package:follow_us/models/response/response_product_list.dart';
import 'package:follow_us/models/response/response_register.dart';
import 'package:follow_us/models/response/response_single_product.dart';

class Provider {

  Dio _dio;
  final String _baseUrl = 'https://followus-api.cleverapps.io/api';

  Future<ResponseLogin> postLogin(RequestLogin requestLogin) async {
    print(
        "user id : ${requestLogin.userId}, password : ${requestLogin.password}");
    _dio = await setupDio();
    String url = '/signin';
    try {
      final response = await _dio.post(url, data: requestLogin.toJson());
      return ResponseLogin.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseLogin.withMessage(e.toString());
    }
  }

  Future<ResponseRegister> postRegister(RequestRegister requestRegister) async {
    print(
        "user id : ${requestRegister.userId}, password : ${requestRegister.password}");
    _dio = await setupDio();
    String url = '/user';
    try {
      final response = await _dio.post(url, data: requestRegister.toJson());
      return ResponseRegister.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseRegister.withMessage(e.toString());
    }
  }

  Future<ResponseCategoryList> getCategoryList() async {
    _dio = await setupDio();
    String url = '/kategori';
    try {
      final response = await _dio.get(url);
      print("response ${response.statusCode} ${response.data}");
      return ResponseCategoryList.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseCategoryList.withMessage(e.toString());
    }
  }

  Future<ResponseProductList> getProductList() async {
    _dio = await setupDio();
    String url = '/produk';
    try {
      final response = await _dio.get(url);
      print("response ${response.statusCode} ${response.data}");
      return ResponseProductList.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseProductList.withMessage(e.toString());
    }
  }

  Future<ResponseSingleProduct> getSingleProduct(String productId) async {
    _dio = await setupDio();
    String url = '/produk/$productId';
    try {
      final response = await _dio.get(url);
      print("response ${response.statusCode} ${response.data}");
      return ResponseSingleProduct.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseSingleProduct.withMessage(e.toString());
    }
  }

  Future<ResponseAddressList> getAddressList() async {
    _dio = await setupDio();
    String url = '/address';
    try {
      final response = await _dio.get(url);
      print("response ${response.statusCode} ${response.data}");
      return ResponseAddressList.fromJson(response.data);
    } on DioError catch (e) {
      return ResponseAddressList.withMessage(e.toString());
    }
  }



  // String _errorHandling(DioError dioError) {
  //   if (dioError.response != null) {
  //     switch (dioError.response.statusCode) {
  //       case 400:
  //         Status modelError = Status.fromJson(dioError.response.data);
  //         return modelError.statusDesc;
  //       case 401:
  //         return "Unauthorized";
  //       case 422:
  //         Status modelError = Status.fromJson(dioError.response.data);
  //         return modelError.statusDesc;
  //       default:
  //         return 'Error occured while Communication with Server with StatusCode : ${dioError.response.statusCode} ${dioError.response.statusMessage}';
  //     }
  //   } else {
  //     return 'Request Time Out';
  //   }
  // }

  Future<Dio> setupDio() async {
    // String _token = '';
    // String userData = await SessionManager().getAuthToken();
    // if (userData != "" && userData != null) {
    //   ResponseLogin responseLogin =
    //   ResponseLogin.fromJson(jsonDecode(userData));
    //   _token = responseLogin.data.token;
    // }
    BaseOptions options = new BaseOptions(
      baseUrl: _baseUrl,
      contentType: Headers.formUrlEncodedContentType,
      // headers: {'Authorization': 'Bearer $_token'},
      connectTimeout: 50000,
      receiveTimeout: 30000,
    );
    return _dio = Dio(options);
  }

}