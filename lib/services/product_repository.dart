import 'package:follow_us/models/response/response.dart';
import 'package:follow_us/services/provider.dart';

class ProductRepository {
  final Provider _provider = Provider();

  Future<ResponseProductList> getProductList() =>
      _provider.getProductList();

  Future<ResponseSingleProduct> getSingleProduct(String productId) =>
      _provider.getSingleProduct(productId);
}
